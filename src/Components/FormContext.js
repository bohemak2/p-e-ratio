import React, { useReducer, createContext } from "react";

// Context within reducer serves similarly to Redux. Provider defines scope of context (better to put it on the top of <App>)

const initState = {};

const reducer = (state, action) => {
    switch (action.type) {
        case "CHANGE":
            return {...state, [action.id]: action.value};
        case "ERASE":
            return {};  
        default:
            throw new Error();
        }
};

export const FormContextProvider = props => {
    const [state, dispatch] = useReducer(reducer, initState);
    
    return <FormContext.Provider value={[state, dispatch]}>{props.children}</FormContext.Provider>;
};

export const FormContext = createContext();