import React from 'react';

import { Form, FormModel, PercentageInput } from './Components/Form';

function PeForm() {
  const { values, onChange, onSubmit } = FormModel("peform", [], _ => { }, _ => { });

  const roeInput = <PercentageInput name="roe" onChange={onChange} value={values.roe}>ROE (rentabilita vlastního kapitálu): </PercentageInput>;
  const iK = <PercentageInput name="iK" onChange={onChange} value={values.iK}>i<sub>k</sub> (požadovaná výnosnost na úrovni vlastního kapitálu): </PercentageInput>;
  const g = <PercentageInput name="g" onChange={onChange} value={values.g}>g (tempo růstu dividend, lze aproximovat za dlouhodobý růst):  </PercentageInput>;
  const inputs = [roeInput, iK, g].map(e => <p>{e}</p>)

  return <><Form inputs={inputs} submit="Vypočítej"><p>Zadejte udaje prosim</p></Form>
    <p>Multiplikátor P/E činí: <b>{Math.round(((1 * (values.g / values.roe)) * (1 + values.g)) / (values.iK - values.g) * 10) / 10 || "zadejte správně všechna data"}</b></p>
  </>
}

export default PeForm;