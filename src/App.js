import React, { useContext } from 'react';

import PeForm from './PeForm';

/* We want know about form in far-parent. Using context aka Redux, Provider must be in parent (index.js). */
import { FormContext } from './Components/FormContext';

function App() {
  const [state, dispatch] = useContext(FormContext);

return <><PeForm /><p>{Object.entries(state).map(([ks, vs]) => <p><b>{ks}</b> {Object.entries(vs).map(([k, v]) => <p>{k} - {v}</p>)}</p>)}</p></>
}

export default App;
