import React, { useState, useContext, useEffect } from 'react';
import { FormContext} from './FormContext'


export const FormModel = (id, init, submit, change) => {
    const [values, setVal] = useState(init);
    const [state, dispatch] = useContext(FormContext);

    const onChange = (e, validate) => {
        const { target } = e;
        const { name, value } = target;

        const validated = validate !== undefined ? validate(value) : value;
        // 3 models for a sake of example (choose only one, otherwise inconsistency between local <> parent <> global) 
        const valuesNew = { ...values, [name]: validated }; 
        // Callback for local state
        setVal(valuesNew);
        // Callback for parent 
        change(values);
        // Callback for 'global' context (can't be in useEffect, causing infinite loop)
        dispatch({ type: "CHANGE", id: id, value: values })
    }
    
    useEffect(()=>{
        // is called after re-render popped out
    })

    const onSubmit = (e) => {
        e.preventDefault();
        submit(values);
    }

    return { values, onChange, onSubmit };
}

/* Props: children (=info before form), inputs */
export const Form = (props) => {
    const submitButton = props.onSubmit !== undefined ?
        <button type="submit">{props.submit || "Odeslat"}</button>
        : <></>;

    return (
        <form onSubmit={props.onSubmit}>
            {props.children}
            {props.inputs}
            {submitButton}
        </form>)
}

/* Props: children (=label), name, value, type, onChange=v -> f, validate=v -> v */
const Input = (props) => {
    return <label>{props.children}<input type={props.type} {...props.attrs} onChange={(e) => props.onChange(e, props.validate)} value={props.value} name={props.name}></input></label>
}

export const TextInput = (props) => {
    return <Input type="text" validate={(v) => typeof v === 'string' ? v : ""} onChange={props.onChange} value={props.value} name={props.name}>{props.children}</Input>
}

export const NumberInput = (props) => {
    return <Input type="number" attrs={{ 'step': 'any', 'min': props.min, 'max': props.max }} validate={(v) => { return parseFloat(v) || 0 }} onChange={props.onChange} value={props.value} name={props.name}>{props.children}</Input>
}

export const PercentageInput = (props) => {
    return <Input type="number" attrs={{ 'step': 0.01, 'min': 0, 'max': 100 }} validate={(v) => { return parseFloat(v) || 0 }} onChange={props.onChange} value={props.value} name={props.name}>{props.children}</Input>
}
